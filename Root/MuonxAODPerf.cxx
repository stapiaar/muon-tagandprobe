#include <AsgTools/MessageCheck.h>
//#include <EventLoop/Job.h>
//#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MuonPerformance/MuonxAODPerf.h>
#include <xAODEventInfo/EventInfo.h>
#include <TSystem.h>
#include <xAODJet/JetContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include "xAODHIEvent/HIEventShapeContainer.h"
#include <xAODMuon/MuonContainer.h>
#include "xAODTracking/TrackParticleContainer.h"
#include <TFile.h>
#include <PATInterfaces/CorrectionCode.h> // to check the return correction code status of tools
#include <xAODCore/ShallowAuxContainer.h>
#include <xAODCore/ShallowCopy.h>
#include <ParticleJetTools/JetFlavourInfo.h>
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include <PathResolver/PathResolver.h>
#include <utility>
#include <TF1.h>


MuonxAODPerf :: MuonxAODPerf (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
      m_grl ("GoodRunsListSelectionTool/grl", this),
      m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool"),	
      m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool"),
      m_muonSelection ("CP::MuonSelectionTool/MuonTightSel", this),
      m_muonSelectionlow ("CP::MuonSelectionTool/MuonLowpTSel", this),
      m_muonCalibrationAndSmearingTool ("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool",this),
      m_trkSelection ("InDet::InDetTrackSelectionTool/MyTrackTool", this),
      m_tmt ("Trig::MatchingTool/MyMatchingTool", this)
{
      m_grl.declarePropertyFor (this, "grlTool"); 
}


StatusCode MuonxAODPerf :: initialize ()
{
  ANA_MSG_INFO ("in initialize");

  TFile *outputFile = wk()->getOutputFile ("myOutput");
  m_tree = new TTree ("tree_trig", "Tree for Trigg eff tag and probe");
  m_tree->SetDirectory (outputFile);
  
  m_tree->Branch ("EventNumber", &m_EventNumber);
  m_tree->Branch ("RunNumber", &m_RunNumber);
  m_tree->Branch ("LumiBlock", &m_LumiBlock);
  // only MC
  m_tree->Branch ("mcEventNumber", &m_mcEventNumber);
  m_tree->Branch ("mcChannelNumber", &m_mcChannelNumber);
  m_tree->Branch ("mcEventWeight", &m_mcEventWeight);
  //Fcal SUM ET
  m_tree->Branch ("Fcal", &m_FCal_Et);
  // Trigger info
  m_tree->Branch ("TriggerObject_Chain", &m_TriggerObject_Chain);
  m_tree->Branch ("TriggerObject_Ps", &m_TriggerObject_Ps);
  // FLOW parameters
  m_tree->Branch("Qx_raw", &FCal_Qx);
  m_tree->Branch("Qy_raw", &FCal_Qy);
  m_tree->Branch("FCalSumEt", &FCal_Et);
  m_tree->Branch("Qx_raw_N", &FCal_Qx_N);
  m_tree->Branch("Qy_raw_N", &FCal_Qy_N);
  m_tree->Branch("FCalSumEt_N", &FCal_Et_N);
  m_tree->Branch("Qx_raw_P", &FCal_Qx_P);
  m_tree->Branch("Qy_raw_P", &FCal_Qy_P);
  m_tree->Branch("FCalSumEt_P", &FCal_Et_P);
  // MUON
  m_tree->Branch ("Muon_pt",     &m_Muon_pt);
  m_tree->Branch ("Muon_eta",    &m_Muon_eta);
  m_tree->Branch ("Muon_phi",    &m_Muon_phi);
  m_tree->Branch ("Muon_charge", &m_Muon_charge);
  m_tree->Branch ("Muon_eloss",  &m_Muon_eloss);
  m_tree->Branch ("Muon_e",      &m_Muon_e);
  m_tree->Branch ("Muon_quality",&m_Muon_quality);
  m_tree->Branch ("Muon_TrigMatchmu4", &m_Muon_TrigMatchmu4);
  m_tree->Branch ("Muon_TrigMatchmu6", &m_Muon_TrigMatchmu6);
  m_tree->Branch ("Dimuon_m", &m_Dimuon_m);
  m_tree->Branch ("Dimuon_pt", &m_Dimuon_pt);
  m_tree->Branch ("Dimuon_eta", &m_Dimuon_eta);
  m_tree->Branch ("Dimuon_phi", &m_Dimuon_phi);
  m_tree->Branch ("Dimuon_rap", &m_Dimuon_rap);
 
  m_tree2 = new TTree ("tree_ms", "Tree for Reco tight|id eff tag and probe");
  m_tree2->SetDirectory (outputFile);
  
  m_tree2->Branch ("EventNumber", &m_EventNumber);
  m_tree2->Branch ("RunNumber", &m_RunNumber);
  m_tree2->Branch ("LumiBlock", &m_LumiBlock);
  // only MC
  m_tree2->Branch ("mcEventNumber", &m_mcEventNumber);
  m_tree2->Branch ("mcChannelNumber", &m_mcChannelNumber);
  m_tree2->Branch ("mcEventWeight", &m_mcEventWeight);
  //Fcal SUM ET
  m_tree2->Branch ("Fcal", &m_FCal_Et);
  // Trigger info
  m_tree2->Branch ("TriggerObject_Chain", &m_TriggerObject_Chain);
  m_tree2->Branch ("TriggerObject_Ps", &m_TriggerObject_Ps);
  // FLOW parameters
  m_tree2->Branch("Qx_raw", &FCal_Qx);
  m_tree2->Branch("Qy_raw", &FCal_Qy);
  m_tree2->Branch("FCalSumEt", &FCal_Et);
  m_tree2->Branch("Qx_raw_N", &FCal_Qx_N);
  m_tree2->Branch("Qy_raw_N", &FCal_Qy_N);
  m_tree2->Branch("FCalSumEt_N", &FCal_Et_N);
  m_tree2->Branch("Qx_raw_P", &FCal_Qx_P);
  m_tree2->Branch("Qy_raw_P", &FCal_Qy_P);
  m_tree2->Branch("FCalSumEt_P", &FCal_Et_P);
  // Trakcs
  m_tree2->Branch ("Ditrack_m",     &m_Ditrack_m);
  m_tree2->Branch ("Ditrack_pt",     &m_Ditrack_pt);
  m_tree2->Branch ("Ditrack_eta",     &m_Ditrack_eta);
  m_tree2->Branch ("Ditrack_phi",     &m_Ditrack_phi);
  m_tree2->Branch ("Ditrack_rap",     &m_Ditrack_rap);
  m_tree2->Branch ("Track_pt",     &m_Track_pt);
  m_tree2->Branch ("Track_eta",     &m_Track_eta);
  m_tree2->Branch ("Track_phi",     &m_Track_phi);
  m_tree2->Branch ("Track_charge",     &m_Track_charge);
  m_tree2->Branch ("Track_e",     &m_Track_e);
  m_tree2->Branch ("Track_matchdR",     &m_Track_matchdR);
  m_tree2->Branch ("Track_matchdRLow",     &m_Track_matchdRLow);

  m_tree3 = new TTree ("tree_id", "Tree for Reco id|ms eff tag and probe");
  m_tree3->SetDirectory (outputFile);

  m_tree3->Branch ("EventNumber", &m_EventNumber);
  m_tree3->Branch ("RunNumber", &m_RunNumber);
  m_tree3->Branch ("LumiBlock", &m_LumiBlock);
  // only MC
  m_tree3->Branch ("mcEventNumber", &m_mcEventNumber);
  m_tree3->Branch ("mcChannelNumber", &m_mcChannelNumber);
  m_tree3->Branch ("mcEventWeight", &m_mcEventWeight);
  //Fcal SUM ET
  m_tree3->Branch ("Fcal", &m_FCal_Et);
  // Trigger info
  m_tree3->Branch ("TriggerObject_Chain", &m_TriggerObject_Chain);
  m_tree3->Branch ("TriggerObject_Ps", &m_TriggerObject_Ps);
  // FLOW parameters
  m_tree3->Branch("Qx_raw", &FCal_Qx);
  m_tree3->Branch("Qy_raw", &FCal_Qy);
  m_tree3->Branch("FCalSumEt", &FCal_Et);
  m_tree3->Branch("Qx_raw_N", &FCal_Qx_N);
  m_tree3->Branch("Qy_raw_N", &FCal_Qy_N);
  m_tree3->Branch("FCalSumEt_N", &FCal_Et_N);
  m_tree3->Branch("Qx_raw_P", &FCal_Qx_P);
  m_tree3->Branch("Qy_raw_P", &FCal_Qy_P);
  m_tree3->Branch("FCalSumEt_P", &FCal_Et_P);
  // Trakcs
  m_tree3->Branch("DiMEtrack_m", &m_DiMEtrack_m);
  m_tree3->Branch("DiMEtrack_pt", &m_DiMEtrack_pt);
  m_tree3->Branch("DiMEtrack_eta", &m_DiMEtrack_eta);
  m_tree3->Branch("DiMEtrack_phi", &m_DiMEtrack_phi);
  m_tree3->Branch("DiMEtrack_rap", &m_DiMEtrack_rap);
  m_tree3->Branch("METrack_pt", &m_METrack_pt);
  m_tree3->Branch("METrack_eta", &m_METrack_eta);
  m_tree3->Branch("METrack_phi", &m_METrack_phi);
  m_tree3->Branch("METrack_charge", &m_METrack_charge);
  m_tree3->Branch("METrack_e", &m_METrack_e);
  m_tree3->Branch("METrack_matchdR", &m_METrack_matchdR);

  m_tree4 = new TTree ("tree_truth", "Tree for Reco truth");
  m_tree4->SetDirectory (outputFile);
  m_tree4->Branch ("EventNumber", &m_EventNumber);
  m_tree4->Branch ("RunNumber", &m_RunNumber);
  m_tree4->Branch ("LumiBlock", &m_LumiBlock);
  // only MC
  m_tree4->Branch ("mcEventNumber", &m_mcEventNumber);
  m_tree4->Branch ("mcChannelNumber", &m_mcChannelNumber);
  m_tree4->Branch ("mcEventWeight", &m_mcEventWeight);
  //Fcal SUM ET
  m_tree4->Branch ("Fcal", &m_FCal_Et);
  //truth muon
  m_tree4->Branch ("TruthMuon_pt", &m_TruthMuon_pt);
  m_tree4->Branch ("TruthMuon_eta", &m_TruthMuon_eta);
  m_tree4->Branch ("TruthMuon_phi", &m_TruthMuon_phi);
  m_tree4->Branch ("TruthMuon_charge", &m_TruthMuon_charge);
  m_tree4->Branch ("TruthMuon_pdgId", &m_TruthMuon_pdgId);
  m_tree4->Branch ("TruthMuon_barcode", &m_TruthMuon_barcode);
  m_tree4->Branch ("TruthMuon_status", &m_TruthMuon_status);
  m_tree4->Branch ("TruthMuon_matchOn", &m_TruthMuon_matchOn);

  // setting GRL 
  std::string GRLFilePath2017 = PathResolverFindCalibFile("MuonPerformance/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml");
  std::string GRLFilePath2018 = PathResolverFindCalibFile("MuonPerformance/data18_hi.periodAllYear_DetStatus-v104-pro22-08_Unknown_PHYS_HeavyIonP_All_Good.xml");
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(GRLFilePath2017);
  vecStringGRL.push_back(GRLFilePath2018);
  ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
  ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
  ANA_CHECK(m_grl.initialize());
  // Trigger configuration
 /* ANA_CHECK (m_trigConfigTool.initialize());
  // Trigger decision tool
  ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle()));
  ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
  ANA_CHECK (m_trigDecisionTool.initialize());
  // trigger matching tool
  ANA_CHECK (m_tmt.setProperty("TrigDecisionTool",m_trigDecisionTool.getHandle()));
  ANA_CHECK (m_tmt.initialize());*/
  // muon selector tool
  ANA_CHECK (m_muonSelection.setProperty("TrtCutOff",true));
  ANA_CHECK (m_muonSelection.setProperty("MaxEta", 2.5)); 
  ANA_CHECK (m_muonSelection.setProperty("MuQuality", 0));
  ANA_CHECK (m_muonSelection.initialize());
  // muon selector tool
  ANA_CHECK (m_muonSelectionlow.setProperty("TrtCutOff",true));
  ANA_CHECK (m_muonSelectionlow.setProperty("MaxEta",2.5));
  ANA_CHECK (m_muonSelectionlow.setProperty("MuQuality",1));
//  ANA_CHECK (m_muonSelectionlow.setProperty("UseMVALowPt",true));
  ANA_CHECK (m_muonSelectionlow.initialize());
  // muon calibration and smearing
  ANA_CHECK (m_muonCalibrationAndSmearingTool.initialize());
  // tracking selection tool
  ANA_CHECK (m_trkSelection.setProperty("CutLevel","TightPrimary"));
  ANA_CHECK (m_trkSelection.setProperty("maxZ0SinTheta",1.0));
  ANA_CHECK (m_trkSelection.setProperty("minPt",1.));
  ANA_CHECK (m_trkSelection.setProperty("maxNSiSharedModules",100));
  ANA_CHECK (m_trkSelection.initialize());


  return StatusCode::SUCCESS;
}

StatusCode MuonxAODPerf :: execute ()
{
  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
   m_EventNumber = eventInfo->eventNumber(); 
   m_RunNumber   = eventInfo->runNumber();
   m_LumiBlock   = eventInfo->lumiBlock();
   m_FCal_Et = -99; 
 
   bool isMC = true;
   // check if the event is MC
   if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
      isMC = true; // can do something with this later
   }

   if(isMC){ 
    m_mcEventNumber         = eventInfo->mcEventNumber();
    m_mcChannelNumber       = eventInfo->mcChannelNumber();
    m_mcEventWeight         = eventInfo->mcEventWeight();
    m_mcSumWight += m_mcEventWeight;
}

// if data check if event passes GRL
if (!isMC) { // it's data!
    if (!m_grl->passRunLB(*eventInfo)) {
      //ANA_MSG_INFO ("drop event: GRL");
      return StatusCode::SUCCESS; // go to next event
    }

    if ((eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ||
        (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
        (eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) ||
        (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))) {
      return StatusCode::SUCCESS; // go to the next event
    } // end if event flags check
  } // end if not MC

    // Saving event plane variables
    if(!isMC) EventPlaneAnalysis();
    // only to be used for PbPb data
    const xAOD::HIEventShapeContainer *hiue(0);
    ANA_CHECK( evtStore()->retrieve(hiue, "CaloSums"));
    double m_fcalEt = hiue->at(5)->et();
    m_FCal_Et = m_fcalEt*1e-6;

    bool dum_HLT_mu3 = false;
    bool dum_HLT_mu8 = false;
    bool dum_HLT_mu10 = false;
    bool dum_HLT_mu14 = false;
    
   /* auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_.*");
    for (auto &trig : chainGroup->getListOfTriggers()) {
    auto cg = m_trigDecisionTool->getChainGroup(trig);
    if(cg->isPassed()) {
      std::string thisTrig = trig;
      m_TriggerObject_Chain.push_back(thisTrig);
      m_TriggerObject_Ps.push_back(cg->getPrescale());

      if(trig=="HLT_mu3")  dum_HLT_mu3 = true;
      if(trig=="HLT_mu8")  dum_HLT_mu8 = true;
      if(trig=="HLT_mu10") dum_HLT_mu10 = true;
      if(trig=="HLT_mu14") dum_HLT_mu14 = true;

	}
    } */

 //if( !(dum_HLT_mu3 || dum_HLT_mu8 || dum_HLT_mu10 || dum_HLT_mu14)  ) return StatusCode::SUCCESS;
 //if( !dum_HLT_mu14 ) return StatusCode::SUCCESS;


  if (isMC) {
       const xAOD::TruthParticleContainer* truthParticles = 0;
       ANA_CHECK(evtStore()->retrieve( truthParticles,"TruthParticles" ));
       for (auto truthMuon: *truthParticles) {
           if (truthMuon->absPdgId() != 13) continue;
           if (truthMuon->status() != 1) continue;
           if (truthMuon->nParents() < 1) continue; 
           const xAOD::TruthParticle* parent = truthMuon->parent(0);
           if ( !(parent->pdgId() == 443)  ) continue;
           //if ( !(parent->pdgId() == 553)  ) continue;
           m_TruthMuon_pt.push_back(truthMuon->pt()*1e-3);
           m_TruthMuon_eta.push_back(truthMuon->eta());
           m_TruthMuon_phi.push_back(truthMuon->phi());
           m_TruthMuon_charge.push_back(truthMuon->charge());
           m_TruthMuon_pdgId.push_back(truthMuon->pdgId());
           m_TruthMuon_barcode.push_back(truthMuon->barcode());
           m_TruthMuon_status.push_back(truthMuon->status());
	   m_TruthMuon_matchOn.push_back( mindRmuon2(truthMuon) );
      }
 }


 const xAOD::MuonContainer* muons = 0;
 ANA_CHECK(evtStore()->retrieve( muons, "Muons" ));
 
 auto muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
 std::unique_ptr<xAOD::MuonContainer> muonsSC (muons_shallowCopy.first);
 std::unique_ptr<xAOD::ShallowAuxContainer> muonsAuxSC (muons_shallowCopy.second);

 //for (auto muonSC : *muonsSC) {
 for (unsigned int i = 0; i < muonsSC->size(); i++) {
 	xAOD::Muon* muonSC0 = (xAOD::Muon*) muonsSC->at(i);
  	if(m_muonCalibrationAndSmearingTool->applyCorrection(*muonSC0)!= CP::CorrectionCode::Ok){
       		ANA_MSG_INFO ("execute(): Problem with Muon Calibration And Smearing Tool (Error or OutOfValidityRange) ");
  	}
  	if ( !m_muonSelection->accept(*muonSC0) ) continue;
        if(isMC){
		const xAOD::TruthParticle* Truth_Muon = xAOD::TruthHelpers::getTruthParticle(*muonSC0);
	        if(!Truth_Muon) {
			ANA_MSG_INFO ("execute(): can't find truth link"); 
			continue;
		}
	}
        //bool dum_fire_mu3 = m_tmt->match( *muonSC0, "HLT_mu3", 0.05);
  	//bool dum_fire_mu8 = m_tmt->match( *muonSC0, "HLT_mu8", 0.05);  
  	//bool dum_fire_mu10 = m_tmt->match( *muonSC0, "HLT_mu10", 0.05);
  	//bool dum_fire_mu14 = m_tmt->match( *muonSC0, "HLT_mu14", 0.05);
  	//if( !dum_fire_mu14 ) continue;
        //if( !(dum_fire_mu3 || dum_fire_mu8 || dum_fire_mu10 || dum_fire_mu14) ) continue;
        // start probe loop
  	for (unsigned int j = i + 1; j < muonsSC->size(); j++) {
	xAOD::Muon* muonSC1 = (xAOD::Muon*) muonsSC->at(j);
		if(m_muonCalibrationAndSmearingTool->applyCorrection(*muonSC1)!= CP::CorrectionCode::Ok){
	     		ANA_MSG_INFO ("execute(): Problem with Muon Calibration And Smearing Tool (Error or OutOfValidityRange) ");
		}	
		if ( !m_muonSelection->accept(*muonSC1) ) continue;
                if (muonSC1->charge() == muonSC0->charge()) continue;

                TLorentzVector daughter0;
         	TLorentzVector daughter1;
   	        TLorentzVector mom;
         	daughter0.SetPtEtaPhiM(muonSC0->pt()/1.e3, muonSC0->eta(), muonSC0->phi(), 0.106);
                daughter1.SetPtEtaPhiM(muonSC1->pt()/1.e3, muonSC1->eta(), muonSC1->phi(), 0.106);
         	mom = daughter0 + daughter1;
                if (mom.M() < 2.7 or mom.M() > 3.5) continue;
                //if (mom.M() < 8.0 or mom.M() > 11.0) continue;
                // Dimuon info
		m_Dimuon_m      .push_back( mom.M());
                m_Dimuon_pt	.push_back( mom.Pt());
                m_Dimuon_eta    .push_back( mom.Eta());
                m_Dimuon_phi    .push_back( mom.Phi());
		m_Dimuon_rap    .push_back( mom.Rapidity());
		//Probe info
		m_Muon_pt       .push_back( muonSC1->pt()*1e-3);
		m_Muon_eta      .push_back( muonSC1->eta() );
	        m_Muon_phi      .push_back( muonSC1->phi() );
  		m_Muon_charge   .push_back( muonSC1->charge() );
		m_Muon_e        .push_back( muonSC1->e()*1e-3 );
		m_Muon_eloss    .push_back( muonSC1->floatParameter(xAOD::Muon::EnergyLoss)*1e-3 );
	        m_Muon_quality  .push_back( m_muonSelection->getQuality(*muonSC1) );
	//	m_Muon_TrigMatchmu4  .push_back( m_tmt->match( *muonSC1, "HLT_mu4", 0.02, true) );
        //        m_Muon_TrigMatchmu6  .push_back( m_tmt->match( *muonSC1, "HLT_mu6", 0.02, true) );

  	} // end loop probe muon

	// start track probe loop
	const xAOD::TrackParticleContainer* tracks = 0;
	ANA_CHECK(evtStore()->retrieve( tracks, "InDetTrackParticles" ));
	for (auto track: *tracks) {
		if ( !m_muonSelection->passedIDCuts(*track) ) continue;
		if ( track->pt()/1e3 < 3) continue;
		if (track->charge() == muonSC0->charge()) continue;
		TLorentzVector daughter0;
         	TLorentzVector daughter1;
   	        TLorentzVector mom;
         	daughter0.SetPtEtaPhiM(muonSC0->pt()/1.e3, muonSC0->eta(), muonSC0->phi(), 0.106);
         	daughter1.SetPtEtaPhiM(track->pt()/1.e3, track->eta(), track->phi(), 0.106);
         	mom = daughter0 + daughter1;
         	if (mom.M() < 2.5 or mom.M() > 3.7) continue;
		//if (mom.M() < 8.0 or mom.M() > 11.0) continue;
		// Dimuon info
		m_Ditrack_m      .push_back( mom.M());
		m_Ditrack_pt     .push_back( mom.Pt());
		m_Ditrack_eta    .push_back( mom.Eta());
		m_Ditrack_phi    .push_back( mom.Phi());
		m_Ditrack_rap    .push_back( mom.Rapidity());
		//Probe info
		m_Track_pt       .push_back( track->pt()*1e-3);
		m_Track_eta      .push_back( track->eta() );
		m_Track_phi      .push_back( track->phi() );
		m_Track_charge   .push_back( track->charge() );
		m_Track_e        .push_back( track->e()*1e-3 );
		m_Track_matchdR  .push_back( mindRmuon(track, m_muonSelection) );
                m_Track_matchdRLow  .push_back( mindRmuon(track, m_muonSelectionlow) );
		
	}// end loop probe track
	
	// start ME probe loop
	const xAOD::TrackParticleContainer* metracks = 0;
        ANA_CHECK(evtStore()->retrieve( metracks, "ExtrapolatedMuonTrackParticles" ));
        for (auto metrack: *metracks) {
                if (metrack->charge() == muonSC0->charge()) continue;
                TLorentzVector daughter0;
                TLorentzVector daughter1;
                TLorentzVector mom;
                daughter0.SetPtEtaPhiM(muonSC0->pt()/1.e3, muonSC0->eta(), muonSC0->phi(), 0.106);
                daughter1.SetPtEtaPhiM(metrack->pt()/1.e3, metrack->eta(), metrack->phi(), 0.106);
                mom = daughter0 + daughter1;
                if (mom.M() < 2.7 or mom.M() > 3.5) continue;
		//if (mom.M() < 8.0 or mom.M() > 11.0) continue;
                // Dimuon info
                m_DiMEtrack_m      .push_back( mom.M());
                m_DiMEtrack_pt     .push_back( mom.Pt());
                m_DiMEtrack_eta    .push_back( mom.Eta());
                m_DiMEtrack_phi    .push_back( mom.Phi());
                m_DiMEtrack_rap    .push_back( mom.Rapidity());
                //Probe info
                m_METrack_pt       .push_back( metrack->pt()*1e-3);
                m_METrack_eta      .push_back( metrack->eta() );
                m_METrack_phi      .push_back( metrack->phi() );
                m_METrack_charge   .push_back( metrack->charge() );
                m_METrack_e        .push_back( metrack->e()*1e-3 );
                m_METrack_matchdR  .push_back( mindRTrk(metrack) );

        }// end loop probe metrack
	
} // end loop Tag
  if(!m_Dimuon_m.empty()) m_tree->Fill();
  if(!m_Ditrack_m.empty()) m_tree2->Fill();
  if(!m_DiMEtrack_m.empty()) m_tree3->Fill();
  if(!m_TruthMuon_pt.empty()) m_tree4->Fill();
  clearVector(); 

  return StatusCode::SUCCESS;
}

void MuonxAODPerf :: clearVector () {
  // trigger T&P
  m_Muon_pt.clear();
  m_Muon_eta.clear();
  m_Muon_phi.clear();
  m_Muon_charge.clear();
  m_Muon_eloss.clear();
  m_Muon_quality.clear();
  m_Muon_e.clear();
  m_Muon_TrigMatchmu4.clear(); 
  m_Muon_TrigMatchmu6.clear();
  m_Dimuon_m.clear();
  m_Dimuon_pt.clear();
  m_Dimuon_eta.clear();
  m_Dimuon_phi.clear();
  m_Dimuon_rap.clear();
  // tight|id T&P
  m_Ditrack_m.clear();
  m_Ditrack_pt.clear();
  m_Ditrack_eta.clear();
  m_Ditrack_phi.clear();
  m_Ditrack_rap.clear();
  m_Track_pt.clear();
  m_Track_eta.clear();
  m_Track_phi.clear();
  m_Track_charge.clear();
  m_Track_e.clear();
  m_Track_matchdR.clear();
  m_Track_matchdRLow.clear();
  // id|ms T&P
  m_DiMEtrack_m.clear();
  m_DiMEtrack_pt.clear();
  m_DiMEtrack_eta.clear();
  m_DiMEtrack_phi.clear();
  m_DiMEtrack_rap.clear();
  m_METrack_pt.clear();
  m_METrack_eta.clear();
  m_METrack_phi.clear();
  m_METrack_charge.clear();
  m_METrack_e.clear();
  m_METrack_matchdR.clear();
  // common branches
  m_TriggerObject_Chain.clear();
  m_TriggerObject_Ps.clear();
  // truth info
  m_TruthMuon_pt.clear();
  m_TruthMuon_eta.clear();
  m_TruthMuon_phi.clear();
  m_TruthMuon_charge.clear();
  m_TruthMuon_pdgId.clear();
  m_TruthMuon_barcode.clear();
  m_TruthMuon_status.clear();
  m_TruthMuon_matchOn.clear();

}

double MuonxAODPerf :: mindRmuon2(const xAOD::TruthParticle* mu)
{
   double dr_min = 2;
   const xAOD::MuonContainer* muons2 = 0;
   if(evtStore()->retrieve( muons2, "Muons" ).isFailure()){
        std::cout << "Could not retrieve MuonContainer with key " << "Muons" << std::endl;
   }

   auto muons_shallowCopy2 = xAOD::shallowCopyContainer( *muons2 );
   std::unique_ptr<xAOD::MuonContainer> muonsSC2 (muons_shallowCopy2.first);
   std::unique_ptr<xAOD::ShallowAuxContainer> muonsAuxSC2 (muons_shallowCopy2.second);

   for (unsigned int i = 0; i < muonsSC2->size(); i++) {

        xAOD::Muon* muon2 = (xAOD::Muon*) muonsSC2->at(i);
        if(m_muonCalibrationAndSmearingTool->applyCorrection(*muon2)!= CP::CorrectionCode::Ok){
                ANA_MSG_INFO ("execute(): Problem with Muon Calibration And Smearing Tool (Error or OutOfValidityRange)");
         }
        if ( !m_muonSelection->accept(*muon2) ) continue;
        if( muon2->charge() != mu->charge()) continue;
        double dr = dR(mu->eta(), mu->phi(), muon2->eta(), muon2->phi());
 
        if(dr<dr_min) dr_min = dr;
   }
 
  return dr_min;
}

double MuonxAODPerf :: mindRmuon(const xAOD::TrackParticle* trk, asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelTool)
{
   double dr_min = 2;
   const xAOD::MuonContainer* muons2 = 0;
   if(evtStore()->retrieve( muons2, "Muons" ).isFailure()){
   	std::cout << "Could not retrieve MuonContainer with key " << "Muons" << std::endl;
   }

   auto muons_shallowCopy2 = xAOD::shallowCopyContainer( *muons2 );
   std::unique_ptr<xAOD::MuonContainer> muonsSC2 (muons_shallowCopy2.first);
   std::unique_ptr<xAOD::ShallowAuxContainer> muonsAuxSC2 (muons_shallowCopy2.second);

   for (unsigned int i = 0; i < muonsSC2->size(); i++) {

	xAOD::Muon* muon2 = (xAOD::Muon*) muonsSC2->at(i);
        if(m_muonCalibrationAndSmearingTool->applyCorrection(*muon2)!= CP::CorrectionCode::Ok){
        	ANA_MSG_INFO ("execute(): Problem with Muon Calibration And Smearing Tool (Error or OutOfValidityRange) ");
        }
        //if ( !m_muonSelection->accept(*muon2) ) continue;
        if ( !m_muonSelTool->accept(*muon2) ) continue;
        if( muon2->charge() != trk->charge()) continue;
   	double dr = dR(trk->eta(), trk->phi(), muon2->eta(), muon2->phi());

        if(dr<dr_min) dr_min = dr;
   }

  return dr_min;
}

double MuonxAODPerf :: mindRTrk(const xAOD::TrackParticle* metrk)
{  
   	double dr_min = 2;
   	const xAOD::TrackParticleContainer* tracks = 0;
   	if(evtStore()->retrieve( tracks, "InDetTrackParticles" ).isFailure()){
    	std::cout << "Could not retrieve MuonContainer with key " << "InDetTrackParticles" << std::endl;
  	}

   	for (auto track: *tracks) {
   		if ( !m_muonSelection->passedIDCuts(*track) ) continue;
        if( track->charge() != metrk->charge()) continue;
        double dr = dR(metrk->eta(), metrk->phi(), track->eta(), track->phi());
		if(dr<dr_min) dr_min = dr;
	}
  
  return dr_min;
}


double MuonxAODPerf :: dR(const double eta1,
                          const double phi1,
                          const double eta2,
                          const double phi2)
   {
     double deta = fabs(eta1 - eta2);
     double dphi = fabs(phi1 - phi2) < TMath::Pi() ? fabs(phi1 - phi2) : 2*TMath::Pi() - fabs(phi1 - phi2);
     return sqrt(deta*deta + dphi*dphi);
   }


StatusCode MuonxAODPerf :: finalize ()
{
  ANA_MSG_INFO ("in finalize");
  return StatusCode::SUCCESS;
}


void MuonxAODPerf :: EventPlaneAnalysis() {
    //xAOD::TEvent* event = wk()->xaodEvent();
    FCal_Et = 0.0;
    FCal_Qx = 0.0;
    FCal_Qy = 0.0;
    FCal_Et_N = 0.0;
    FCal_Qx_N = 0.0;
    FCal_Qy_N = 0.0;
    FCal_Et_P = 0.0;
    FCal_Qx_P = 0.0;
    FCal_Qy_P = 0.0;
    //string m_HIEventShapeContainerKey ="HIEventShape";
    const xAOD::HIEventShapeContainer* l_HIevtShapeContainer;
    if(evtStore()->retrieve(l_HIevtShapeContainer,"HIEventShape").isFailure()) {
        std::cout << "Could not retrieve HIEVentSHapeContainer with key " << "HIEventShape" << std::endl;
        return;
    }
    int size=l_HIevtShapeContainer->size();
    for(int ish=0;ish<size;ish++){
        const xAOD::HIEventShape *sh=l_HIevtShapeContainer->at(ish);
        int layer=sh->layer();
        if(layer!=21 && layer!=22 && layer!=23) continue; //select on the FCal
        const std::vector<float> &c1 = sh->etCos(); //The cosine moments of the ET distribution in the FCalrimeter in 0.1 eta-slices
        const std::vector<float> &s1 = sh->etSin(); //The sine   moments of the ET distribution in the FCalrimeter in 0.1 eta-slices
        if (sh->etaMin() < 0.0) {
            // Negative Eta
            FCal_Et_N += sh->et(); //The ET in the 0.1 eta-slice 
            FCal_Qx_N += c1[1]; // the "[1]" selects harmonic=2 modulation
            FCal_Qy_N += s1[1]; // the "[1]" selects harmonic=2 modulation
        }
        if (sh->etaMin() > 0.0) {
            // Positive Eta
            FCal_Et_P += sh->et(); //The ET in the 0.1 eta-slice 
            FCal_Qx_P += c1[1]; // the "[1]" selects harmonic=2 modulation
            FCal_Qy_P += s1[1]; // the "[1]" selects harmonic=2 modulation
        }
        FCal_Et += sh->et(); //The ET in the 0.1 eta-slice 
        FCal_Qx += c1[1]; // the "[1]" selects harmonic=2 modulation
        FCal_Qy += s1[1]; // the "[1]" selects harmonic=2 modulation

    }
    FCal_Qx /= FCal_Et; //divide by the ET
    FCal_Qy /= FCal_Et; //divide by the ET
    FCal_Et *= 1e-6; // SumEt in TeV
    FCal_Qx_N /= FCal_Et_N; //divide by the ET
    FCal_Qy_N /= FCal_Et_N; //divide by the ET
    FCal_Et_N *= 1e-6; // SumEt in TeV
    FCal_Qx_P /= FCal_Et_P; //divide by the ET
    FCal_Qy_P /= FCal_Et_P; //divide by the ET
    FCal_Et_P *= 1e-6; // SumEt in TeV
}

int MuonxAODPerf :: parent_classify(const xAOD::TruthParticle *theParticle) {
  const xAOD::TruthParticle *parent = 0; // the parent object
  Int_t particle_id = 999;
  Int_t parent_id = 999;

  if (theParticle == NULL) return parent_id;

  particle_id = theParticle->pdgId();
  parent = theParticle->parent(0);
  if (parent) parent_id = parent->pdgId();
  else return parent_id;

  while (fabs(parent_id) == fabs(particle_id) && fabs(parent_id) < 400 && fabs(parent_id) != 0) {
    parent = parent->parent(0);
    if (parent) parent_id = parent->pdgId();
    else break;
  }
  return parent_id;
}

