#ifndef MuonPerformance_MuonxAODPerf_H
#define MuonPerformance_MuonxAODPerf_H

#include <AnaAlgorithm/AnaAlgorithm.h>
// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include <xAODJet/JetContainer.h>
#include <JetCalibTools/IJetCalibrationTool.h>
#include <TriggerMatchingTool/IMatchingTool.h>
#include "xAODTruth/TruthParticleContainer.h"

// Others
#include <TTree.h>
#include <TH1.h>
#include <utility>

class MuonxAODPerf : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MuonxAODPerf (const std::string& name, ISvcLocator* pSvcLocator);
  
  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  void  clearVector();
  void  EventPlaneAnalysis();
  double dR(const double eta1,
            const double phi1,
            const double eta2,
            const double phi2);
  int    parent_classify(const xAOD::TruthParticle *theParticle);
  double mindRmuon2(const xAOD::TruthParticle* mu);
  double mindRmuon(const xAOD::TrackParticle* trk, asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelTool);
  double mindRTrk(const xAOD::TrackParticle* metrk);
  
private:
 
 
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool; //!
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; //!
  // MuonSelectionTool
  asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection; //!
  asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionlow; //!
  // MuonCalibrationAndSmearing
  asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool; //!
  asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelection; //!
  asg::AnaToolHandle<Trig::IMatchingTool> m_tmt; //!

  std::string m_outputName;
  std::string m_outputHist;
  float m_mcSumWight = 0;
  TTree *m_tree; //!
  TTree *m_tree2; //!
  TTree *m_tree3; //!
  TTree *m_tree4; //!
  int m_EventNumber; //!
  int m_RunNumber; //!
  int m_LumiBlock; //!
  int m_mcEventNumber; //!
  int m_mcChannelNumber; //!
  float m_mcEventWeight; //!

  double m_FCal_Et; //!

  double FCal_Qx; //!
  double FCal_Qy; //!
  double FCal_Et; //!
  double FCal_Qx_N; //!
  double FCal_Qy_N; //!
  double FCal_Et_N; //!
  double FCal_Qx_P; //!
  double FCal_Qy_P; //!
  double FCal_Et_P; //!

  std::vector<std::string> m_TriggerObject_Chain; //!
  std::vector<float>   m_TriggerObject_Ps; //!
  
  // Trigger Tag and probe members
  std::vector<double> m_Muon_pt; //!
  std::vector<double> m_Muon_eta; //!
  std::vector<double> m_Muon_phi; //!
  std::vector<int>    m_Muon_charge; //!
  std::vector<double> m_Muon_eloss; //!
  std::vector<double> m_Muon_e; //!
  std::vector<double> m_Muon_quality; //!
  std::vector<bool>   m_Muon_TrigMatchmu4; //!
  std::vector<bool>   m_Muon_TrigMatchmu6; //!
  std::vector<double> m_Dimuon_m; //!
  std::vector<double> m_Dimuon_pt; //!
  std::vector<double> m_Dimuon_eta; //!
  std::vector<double> m_Dimuon_phi; //!
  std::vector<double> m_Dimuon_rap; //!
  // Tight|ID Tag and probe members
  std::vector<double> m_Ditrack_m; //!
  std::vector<double> m_Ditrack_pt; //!
  std::vector<double> m_Ditrack_eta; //!
  std::vector<double> m_Ditrack_phi; //!
  std::vector<double> m_Ditrack_rap; //!
  std::vector<double> m_Track_pt; //!
  std::vector<double> m_Track_eta; //!
  std::vector<double> m_Track_phi; //!
  std::vector<int>    m_Track_charge; //!
  std::vector<double> m_Track_e; //!
  std::vector<double> m_Track_matchdR; //!
  std::vector<double> m_Track_matchdRLow; //!
  // ID|MS Tag and probe members
  std::vector<double> m_DiMEtrack_m; //!
  std::vector<double> m_DiMEtrack_pt; //!
  std::vector<double> m_DiMEtrack_eta; //!
  std::vector<double> m_DiMEtrack_phi; //!
  std::vector<double> m_DiMEtrack_rap; //!
  std::vector<double> m_METrack_pt; //!
  std::vector<double> m_METrack_eta; //!
  std::vector<double> m_METrack_phi; //!
  std::vector<int> m_METrack_charge; //!
  std::vector<double> m_METrack_e; //!
  std::vector<double> m_METrack_matchdR; //!
  // truth info
  std::vector<double> m_TruthMuon_pt; //!
  std::vector<double> m_TruthMuon_eta; //!
  std::vector<double> m_TruthMuon_phi; //!
  std::vector<double> m_TruthMuon_charge; //!
  std::vector<double> m_TruthMuon_pdgId; //!
  std::vector<double> m_TruthMuon_barcode; //!
  std::vector<double> m_TruthMuon_status; //!
  std::vector<double> m_TruthMuon_matchOn; //!
   

//
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
  };
  
 #endif
